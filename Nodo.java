import java.util.*;
public class Nodo{
	private Vector<Nodo> hijos = new Vector<Nodo>(1,1);
	private Puzzle nodo;
	private boolean visitado   = false;
	
	public Nodo(Puzzle nodo){
		this.nodo 	= nodo;
	}

	public void setSon(Nodo nodo){
		hijos.add(nodo);
	}

	public Vector<Nodo> getChildren(){
		return hijos;
	}

	public Puzzle getNodo(){
		return nodo;
	}

	public boolean getVisitado(){
		return visitado;
	}

	public void setVisitado(boolean flag){
		visitado = flag;
	}

	public String toString(){
		return nodo.toString();
	}
}