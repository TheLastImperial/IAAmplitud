import java.util.*;
public class Arbol{
	private Nodo root;
	private Map<String, Nodo> arbol;
	public Arbol(Puzzle p){
		root = new Nodo(p);
	}
	public void generaArbol(){
		/*
			La variable arbol permite saber que nodos han sido agregados con lo cual se 
			evita que se repitan notos.
		*/
		arbol = new HashMap<String, Nodo>();
		Stack<Nodo> pila = new Stack();
		pila.push(root);
		System.out.println(root.getNodo());
		while( !pila.empty() ){
			Nodo nodo = pila.pop();
			/*
				El metodo isObjetive nos devuelve true cuando el puzzle tiene
				las caracteristicas necesarias para decir que es el estado
				final.
			*/
			if( nodo.getNodo().isObjetive() ){
				System.out.println(nodo.getNodo().toString());
				arbol.put(nodo.toString(), nodo );
				break;
			}
			else{
				/*
					El metodo getStates devuelve un vector de puzzles los 
					cuales representan los movimientos que puede realizar el puzzle
				*/
				Vector<Puzzle> vP = nodo.getNodo().getStates();
				for(int i = 0; i<vP.size(); i++){
					Nodo n = new Nodo( vP.get(i) );
					if( n.getNodo().isObjetive() ){
						System.out.println( n.getNodo().toString() );
						nodo.setSon( n );
						arbol.put( n.toString(), n );
						pila.clear();
						break;
					}
					/*
						Preguntamos si el nodo generado ya ha sido agregado a nuestro
						arbol con anterioridad.
					*/
					if( !vP.get(i).equals( root.getNodo() ) && !arbol.containsKey( n.toString() )  ){
						System.out.println( n.getNodo().toString() );
						nodo.setSon( n );
						pila.push(  n );
						arbol.put( n.toString(), n );
					}
				}
			}
		}
	}

/*
			Se realiza la busqueda siguiendo el pseudocodigo que se muestra a continuacion.

			1  método BFS(Grafo,origen):
			2      creamos una cola Q
			3      agregamos origen a la cola Q
			4      marcamos origen como visitado
			5      mientras Q no este vacío:
			6          sacamos un elemento de la cola Q llamado v
			7          para cada vertice w adyacente a v en el Grafo: 
			8              si w no ah sido visitado:
			9                 marcamos como visitado w
			10                 insertamos w dentro de la cola Q
*/

	public void busquedaAmplitud(){
		Vector<Nodo> fila = new Vector<Nodo>(1,1);
		root.setVisitado(true);
		fila.add(root);

		System.out.println("Primera Impresion\n"+ root.getNodo().toString() );

		while( !fila.isEmpty() ){
			if( fila.elementAt(0).getNodo().isObjetive() ){
				System.out.println("Se ha encontrado la solucion");
				System.out.println( fila.elementAt(0).getNodo().toString() );
				break;
			}
			Vector<Nodo> hijos = fila.elementAt(0).getChildren();
			fila.remove(0);

			for(int i = 0; i< hijos.size(); i++){
				/*
					El if determina cuando se encuentra la solucion.
				*/
				System.out.println( "Iteracion:"+i );
				if( hijos.elementAt(i).getNodo().isObjetive() ){
					System.out.println("Se ha encontrado la solucion");
					System.out.println( hijos.elementAt(i).getNodo().toString() );
					break;
				}
				/*
					Nos aseguramos que el nodo no haya sido visitado con anterioridad.
				*/
				if( !hijos.elementAt(i).getVisitado() ){
					System.out.println("Impresion de hijos\n"+ hijos.elementAt(i).getNodo().toString() );
					hijos.elementAt(i).setVisitado( true );
					fila.add(hijos.elementAt(i));
				}
				System.out.println( "Nodo hijo: \n"+hijos.elementAt(i).getNodo().toString() );
			}
		}
	}
	public Vector<Nodo> getArbol(){
		return root.getChildren();
	}
}